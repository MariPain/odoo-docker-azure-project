
## Modernisation de l'Infrastructure d'Entreprise avec Docker : Déploiement Optimisé d'Odoo sur Azure

**Contexte du projet :**

En tant qu'administrateur système DevOps chez La Force Tranquille [LFT], il a pris la décision de moderniser l'infrastructure de déploiement de l'application Odoo, une plateforme de gestion d'entreprise à code ouvert, en adoptant Docker. La société cherche à simplifier le processus de déploiement, à améliorer la flexibilité et à faciliter la gestion de différentes versions d'Odoo.

**Objectifs du projet :**

- Conception d'une architecture Docker adaptée à Odoo, y compris les conteneurs, les réseaux et les volumes.

- Créer des Dockerfiles pour construire des images Odoo personnalisées, en tenant compte des spécificités de l'application et des modules utilisés par l'entreprise.

- Estimer le coût d'exploitation final et optimiser l'infrastructure en termes de taille de réseau, de performance et de sécurité.

**Modalités pédagogiques :**

- Travail individuel avec possibilité de faire appel à des collègues pour clarifier les concepts ou rechercher des informations.

- Utilisation d'Ansible à partir d'Azure Cloud Shell pour créer l'infrastructure initiale.

**Livrables :**

1. Infrastructure Docker fonctionnelle sur Azure.

2. Deux conteneurs Docker exécutés avec succès à partir d'images publiques.

3. Image Docker personnalisée construite à partir d'un Dockerfile.

4. Réseau Docker créé et testé pour la communication entre conteneurs.

5. Volume Docker monté et testé pour la persistance des données.

6. Utilisation de commandes Docker pour gérer les conteneurs, les images, les réseaux et les volumes.

7. Fichier de composition Docker (Docker Compose) décrivant une application multiconteneur.

8. Déploiement réussi de l'application multi-conteneurs à l'aide de Docker Compose.

9. Explication de l'infrastructure globale.

**Critères de performance :**

- Fonctionnalité complète de l'infrastructure.

- Installation et exécution correctes de conteneurs.

- Construction réussie d'images personnalisées et utilisation appropriée de réseaux et de volumes Docker.

- Utilisation efficace des commandes Docker et déploiement correct de l'application multiconteneur.

**Étape 1 : Approche :**

**Procédure :**

- Utilisez Ansible à partir d'Azure Cloud Shell pour créer l'infrastructure initiale dans Azure, y compris une VM Ubuntu, un réseau virtuel, deux sous-réseaux et une paire de clés.

- Installer Docker sur la VM Ubuntu selon les instructions fournies dans la documentation officielle.

- Téléchargez les images Docker nécessaires pour Odoo et PostgreSQL.

- Créer des Dockerfiles personnalisés pour Odoo et PostgreSQL.

- Utiliser Docker Compose pour définir l'architecture de l'application multi-conteneur.

- Tester et vérifier chaque composant de l'infrastructure Docker pour s'assurer de son bon fonctionnement.

**Scrème d'architecture**

![ Azure -> Ubuntu -> Docker -> Odoo](img/azure-docker-odoo.png )

**Étape 2 : Infrastructure :**

Ansible est un outil d'automatisation open-source qui permet la gestion et la configuration de systèmes de manière efficace et cohérente. Utilisez un langage descriptif simple (YAML) pour définir des playbooks qui décrivent les états souhaités des systèmes, ce qui permet l'automatisation de tâches complexes de manière simple.

Azure Cloud Shell est un environnement de shell interactif basé sur le cloud qui fournit un certain nombre d'outils préinstallés, y compris Ansible. Avec Ansible dans Azure Cloud Shell, les utilisateurs peuvent automatiser efficacement les tâches liées à l'infrastructure en nuage Azure.

Pour utiliser Ansible dans Azure Cloud Shell, il peut être nécessaire d'installer des modules Galaxy ou des collections supplémentaires en fonction des exigences spécifiques du projet. Ces modules peuvent être facilement installés en utilisant la commande `ansible-galaxy` suivie du nom du module ou de la collection.

Ansible a été choisi pour ce projet en raison de son agilité dans la création des ressources nécessaires dans Azure. Ansible permet de définir l'infrastructure comme un code, ce qui facilite la reproductibilité et l'automatisation des tâches de mise en œuvre complexes.

**Instructions pour l'utiliser dans le contexte de ce projet :**

1. Nous avons téléchargé le référentiel dans Azure Cloud Shell en utilisant les commandes `git clone https://gitlab.com/MariPain/odoo-docker-azure-project.git`, puis `git pull` pour télécharger la dernière version.

2. Nous avons navigué jusqu'au répertoire `ansible/infra.playbook`.

3. Exécuter le playbook en utilisant la commande `ansible-playbook infra.yml`. Ce playbook sera chargé de créer les ressources nécessaires dans Azure comme défini dans le fichier `infra.yml`.

4. Une fois que le playbook a terminé la création de ressources, connectez-vous à la VM (appelée `odoo-service`) pour passer à la phase suivante du déploiement d'Odoo avec des conteneurs Docker.

Le playbook est chargé de créer l'infrastructure nécessaire dans Azure pour déployer l'application Odoo. Nous allons décrire les ressources qui sont créées en utilisant les variables définies dans le playbook :

1. **Groupe de ressources (Groupe de ressources) :**

- Nom : "OCC_ASD_Maria-Dolores"

- Emplacement : "France Centrale"

- Description : Le groupe de ressources est un conteneur logique utilisé pour organiser et gérer les ressources Azure dans un environnement. Il regroupe les ressources qui partagent le même cycle de vie, les mêmes permis et les mêmes politiques.

2. **Réseau virtuel (réseau virtuel) :**

- Nom : "Vnet_LFT"

- Plage d'adresses : "10.0.16.0/24"

- Description : Un réseau virtuel dans Azure vous permet d'isoler les ressources dans un réseau virtualisé. Ce réseau virtuel offre une connectivité entre les ressources déployées sur Azure.

3. **Sous-réseaux (sous-réseaux) :**

- Sous-réseau Frontend :

- Nom : "Subnet_Front"

- Gamme d'adresses : "10.0.16.0/28"

- Description : Le sous-réseau divise un réseau virtuel en segments plus petits pour gérer le trafic réseau. Le sous-réseau Frontend peut être utilisé pour les ressources publiques, tandis qu'il peut y avoir un autre sous-réseau destiné aux ressources privées.

Dans ce contexte, nous n'utiliserons qu'un seul sous-réseau dans Azure car nous allons à nouveau segmenter les sous-réseaux au sein de l'architecture Docker.

4. **Network Security Group (Groupe de sécurité réseau) :**

- Nom : "NSG_LFT"

- Description : Le groupe de sécurité réseau agit comme un pare-feu virtuel pour contrôler le trafic réseau vers les ressources dans Azure. Des règles d'accès sont définies ici pour autoriser ou refuser le trafic entrant et sortant.

5. **Public IP Address (Adresse IP publique) :**

-"odoo-service-public-ip": L'adresse IP publique est attribuée à l'interface réseau de la VM pour permettre la connectivité à partir d'Internet.

6. **Network Interface (Interface réseau) :**

-"odoo-service-nic" : L'interface réseau connecte la VM au réseau virtuel et fournit une connectivité réseau pour la VM.

7. **Machine virtuelle (machine virtuelle) :**

- Nom : "odoo-service"

- Taille de la VM : "Standard_B2s"

- Image : "Ubuntu Server 20.04 LTS"

- Type de disque géré : "Standard_LRS"

- Description : La machine virtuelle est l'environnement d'exécution pour l'application Odoo. Il est créé avec une VM Ubuntu et configuré avec l'interface réseau, l'adresse IP publique et la clé SSH nécessaires à la connexion à distance.

De cette façon, la création de l'infrastructure nécessaire dans Azure sera automatisée en utilisant Ansible dans Azure Cloud Shell, ce qui permettra une mise en œuvre rapide et cohérente de l'environnement requis pour le déploiement d'Odoo.

**Étape 3 : Déploiement automatisé d'applications avec Docker :**

Une fois que la machine virtuelle est créée et configurée, nous procédons au déploiement automatisé de l'application à l'aide de Docker.

Pour ce faire, nous allons nous connecter à la VM `odoo-service` à partir d'Azure Cloud Shell en utilisant la commande `ssh mariola@[ip_vm]`. Une fois que nous avons accédé à la machine, nous devons cloner à nouveau le référentiel contenant les instructions. Encore une fois `git clone https://gitlab.com/MariPain/odoo-docker-azure-project.git`, puis `git pull` pour télécharger la dernière version.

Nous avons navigué jusqu'au répertoire `docker/` et y avons trouvé plusieurs fichiers. Le premier qui nous intéresse est le script de mise en page appelé `docker_cnf.sh` qui nous facilitera la mise à jour des dépôts, le téléchargement et les installations nécessaires de Docker.


1. **AutomatizaciÃ³n de la ImplementaciÃ³n de Docker en la VM Ubuntu:**

Pour accélérer l'installation et la configuration de Docker, ainsi que la création et la connexion des conteneurs Odoo et PostgreSQL, nous utiliserons un script d'automatisation appelé `docker_cnf.sh`. Ce script effectue les actions suivantes :

- Mettez à jour et installez les paquets nécessaires pour travailler avec Docker.

- Ajoute la clé GPG officielle de Docker et configure le dépôt stable de Docker.

- Installez Docker Engine et Docker Compose.

- Créez des réseaux Docker personnalisés (`app-network` et `db-network`) avec des sous-réseaux spécifiques.

- Exécutez les conteneurs Odoo et PostgreSQL, en les connectant aux réseaux personnalisés créés.

- Vérifie que les conteneurs sont en cours d'exécution et correctement connectés aux réseaux.

3. **Création de Dockerfiles personnalisés (si nécessaire) :**

- Pour la personnalisation des conteneurs, nous avons créé des Dockerfiles personnalisés afin de construire les images à partir de ceux-ci. Cependant, cela signifie, s'il y a des configurations adicuonales nécessaires dans les images. Dans ce projet, deux `dockerfile` ont été créés pour cela, et ils seront utilisés pour le déploiement du service.

4. **Définition de l'architecture multiconteneur avec Docker Compose :**

- Nous utiliserons Docker Compose pour définir l'architecture multiconteneur de l'application Odoo. Pour ce faire, je crée un fichier appelé `docker-compose.yml` dans le répertoire de travail et définit les services Odoo et PostgreSQL, y compris la configuration réseau, les volumes et les variables d'environnement nécessaires au bon fonctionnement.

- Le fichier docker-compose.yml définit l'architecture multiconteneur de l'application Odoo.

- Les services Odoo et PostgreSQL sont définis, en spécifiant l'image Docker à utiliser pour chacun.

- Configuration du réseau et des ports : Les réseaux app-network et db-network sont configurés, en attribuant des adresses IP spécifiques à chaque service.

- Le port 80 de l'hôte est mappé sur le port 8069 du conteneur Odoo pour accéder à l'application à partir du navigateur.

- Spécification de l'attribution d'adresses IP pour les services sur les réseaux `app-network` et `db-network`.

- Mappage du port 80 de l'hôte au port 8069 du conteneur Odoo pour accéder à l'application depuis le navigateur.

5. **Test et vérification de l'infrastructure Docker :**

- Une fois l'architecture multiconteneur définie avec Docker Compose, exécutez la commande `docker-compose up -d` pour démarrer les conteneurs en arrière-plan.

- Vérifiez que les conteneurs sont en cours d'exécution en exécutant la commande `docker-compose ps`.

- Accédez à l'interface web Odoo à partir d'un navigateur en utilisant l'adresse IP de la VM et le port spécifié dans le fichier `docker-compose.yml` (par exemple, `http://<adresse_ip_vm>:80`).

**Considérations de sécurité pour le projet :**

1. **Groupe de sécurité du réseau (NSG) :**

- Le groupe de sécurité réseau (NSG) dans Azure est essentiel pour filtrer le trafic vers et depuis les ressources en nuage, y compris les machines virtuelles et les conteneurs Docker. Lorsque vous configurez des règles dans le NSG, vous limitez le trafic à ce qui est nécessaire, comme l'accès SSH à partir d'adresses IP spécifiques et le trafic d'application (par exemple, le port 8069 pour Odoo). Cela permet de minimiser le risque d'exposition à des menaces extérieures.

2. **VNET et sous-réseaux :**

- L'utilisation d'un réseau virtuel (VNET) dans Azure permet la création de sous-réseaux pour segmenter les ressources en fonction de leur fonction et de leur niveau de confiance. Les sous-réseaux aident à organiser les ressources et à appliquer des politiques de sécurité de manière granulaire. De plus, les règles du NSG s'appliquent au niveau du sous-réseau, ce qui garantit une plus grande sécurité et un meilleur contrôle sur le trafic réseau.

- Les sous-réseaux Docker, tels que `app-network` et `db-network` dans notre cas, agissent de manière similaire aux sous-réseaux dans Azure VNET. Ils permettent de segmenter les services et les conteneurs en groupes logiques et d'appliquer des politiques de sécurité spécifiques à chacun d'eux. En créant ces sous-réseaux, nous mettons en place un périmètre de sécurité supplémentaire pour nos services Docker, en limitant l'exposition à d'éventuelles menaces externes.

3. **Conteneurs Docker :**

- Il est essentiel d'exécuter les conteneurs Docker avec les privilèges minimums nécessaires à leur bon fonctionnement. Cela implique de restreindre les privilèges de l'utilisateur dans le conteneur et d'éviter d'exécuter des conteneurs avec des privilèges root dans la mesure du possible. De plus, les meilleures pratiques de sécurité doivent être suivies lors de la création d'images Docker, telles que la réduction au minimum du nombre de couches, l'analyse des vulnérabilités connues et la mise à jour régulière.

4. **Interface réseau (NIC) :**

- L'interface réseau (NIC) associée à la machine virtuelle ou au conteneur Docker joue un rôle crucial dans la connectivité réseau et la sécurité. Il est important de configurer correctement le NIC pour utiliser le NSG approprié et de restreindre le trafic conformément aux règles définies dans le NSG. Cela garantit que seul le trafic nécessaire est autorisé et que l'exposition est limitée à d'éventuelles attaques.

5. **Redirection des ports :**

- Lorsque vous devez exposer des services dans des conteneurs Docker à des ports spécifiques, il est essentiel de configurer la redirection de port en toute sécurité. Cela implique de limiter l'exposition des ports à ce qui est strictement nécessaire et d'utiliser des techniques telles que la traduction de ports dynamiques pour éviter les attaques de numérisation des ports et réduire la surface d'attaque.

6. **Connetion SSH :**

- Pour protéger la connexion SSH à la machine virtuelle ou au conteneur Docker, il est conseillé d'utiliser des clés SSH plutôt que des mots de passe. Des clés SSH fortes doivent être générées et utilisées, et protégées de manière appropriée pour empêcher les accès non autorisés. En outre, il est recommandé de restreindre l'accès SSH uniquement à des adresses IP spécifiques et de limiter le nombre de tentatives de connexion pour atténuer d'éventuelles attaques par force brute.

**Sécurisation supplémentaire avec Bastion (non mise en œuvre) :**

Si vous souhaitez ajouter une couche de sécurité supplémentaire, vous pouvez déployer un bastiÃ³n dans Azure. Un bastion est un serveur intermédiaire qui agit comme un point d'entrée sécurisé pour accéder aux machines virtuelles et aux conteneurs Docker sur un réseau privé virtuel (VNET) sans les exposer directement à Internet.

L'utilisation d'un bastion permet de réduire la surface d'attaque en fournissant un seul point d'entrée contrôlé et surveillé pour les connexions SSH.

Mais...

**Qu'est-ce que azure bastion ?**

Un bastion, également connu sous le nom d'hôte de saut ou d'hôte de saut, est un serveur configuré pour protéger et contrôler l'accès à d'autres serveurs sur un réseau privé. Il agit comme un point d'entrée unique et sécurisé pour les administrateurs ou les opérateurs de systèmes qui ont besoin d'accéder à des machines sur le réseau interne à partir d'un emplacement externe.

**Fonctionnement du bastion :**

1. **Connexion initiale :** Au lieu d'attribuer une adresse IP publique directement à la VM (dans ce cas, `odoo-service`), nous utiliserons le bastion comme point d'entrée pour accéder au réseau privé où réside la VM.

2. **Connexion SSH via la base :** Lorsqu'un administrateur souhaite accéder à la VM `odoo-service`, il se connecte d'abord à la base en utilisant SSH à partir d'un emplacement externe. Le bastion authentifie l'administrateur et vérifie ses informations d'identification.

3. **Redirection des ports :** Une fois authentifié dans le bastion, l'administrateur peut utiliser SSH pour se connecter à la VM `odoo-service`, mais cette connexion se fait via le bastion. Le bastionn redirige le trafic SSH entrant vers le port correspondant de la VM `odoo-service`.

4. **Sécurité supplémentaire :** Le bastion agit comme un point de contrôle d'accès centralisé, ce qui permet de mettre en œuvre des politiques de sécurité supplémentaires telles que l'authentification à deux facteurs, l'enregistrement d'audit et le filtrage IP.

**Avantages de l'utilisation de bastion :**

- **Sécurité accrue :** En supprimant l'exposition directe de la VM `odoo-service` à Internet, nous réduisons le risque d'attaques malveillantes.

- **Contrôle d'accès :** Le bastion fournit un point centralisé pour gérer et auditer l'accès aux serveurs internes.

- **Simplification de la sécurité :** En concentrant les mesures de sécurité sur un seul point d'entrée, nous simplifions la gestion et le maintien de la sécurité de l'infrastructure.

**Implémentation du bastion :**

Pour mettre en œuvre un bastion dans notre architecture, nous devrons configurer un serveur supplémentaire en tant que bastion et ajuster les règles de pare-feu et les groupes de sécurité pour permettre le trafic SSH uniquement du bastiÃ³n vers la VM `odoo-service`. De plus, nous configurerons le bastiÃ³n pour rediriger le trafic SSH entrant vers le port correspondant de la VM `odoo-service`.

<details>
<summary>ES</summary>

   <h3>Modernización de Infraestructura Empresarial con Docker: Despliegue Optimizado de Odoo en Azure</h3>

   **Contexto del Proyecto:**
   Como Administrador de Sistemas DevOps en La Force Tranquille [LFT], se ha tomado la decisión de modernizar la infraestructura de despliegue de la aplicación Odoo, una plataforma de gestión empresarial de código abierto, adoptando Docker. La empresa busca simplificar el proceso de despliegue, mejorar la flexibilidad y facilitar la gestión de diferentes versiones de Odoo.

   **Objetivos del Proyecto:**
   - Diseñar una arquitectura Docker adaptada a Odoo, incluyendo contenedores, redes y volúmenes.
   - Crear Dockerfiles para construir imágenes personalizadas de Odoo, considerando las especificidades de la aplicación y los módulos utilizados por la empresa.
   - Estimar el costo de funcionamiento final y optimizar la infraestructura en términos de tamaño de red, rendimiento y seguridad.

   **Modalidades Pedagógicas:**
   - Trabajo individual con posibilidad de recurrir a colegas para aclarar conceptos o buscar información.
   - Uso de Ansible desde Azure Cloud Shell para crear la infraestructura inicial.

   **Entregables:**
   1. Infraestructura Docker funcional en Azure.
   2. Dos contenedores Docker ejecutados con éxito desde imágenes públicas.
   3. Imagen Docker personalizada construida a partir de un Dockerfile.
   4. Red Docker creada y probada para la comunicación entre contenedores.
   5. Volumen Docker montado y probado para la persistencia de datos.
   6. Uso de comandos Docker para gestionar contenedores, imágenes, redes y volúmenes.
   7. Archivo de composición Docker (Docker Compose) describiendo una aplicación multicontenedor.
   8. Despliegue exitoso de la aplicación multicontenedor utilizando Docker Compose.
   9. Explicación de la infraestructura global.

   **Criterios de Rendimiento:**
   - Funcionalidad completa de la infraestructura.
   - Correcta instalación y ejecución de contenedores.
   - Construcción exitosa de imágenes personalizadas y uso adecuado de redes y volúmenes Docker.
   - Utilización efectiva de comandos Docker y correcto despliegue de la aplicación multicontenedor.

   **Etapa 1: Planteamiento:**

   **Procedimiento:**

   - Utilizar Ansible desde Azure Cloud Shell para crear la infraestructura inicial en Azure, incluyendo una VM Ubuntu, una red virtual, dos subredes y un par de claves.
   - Instalar Docker en la VM Ubuntu según las instrucciones proporcionadas en la documentación oficial.
   - Descargar las imágenes de Docker necesarias para Odoo y PostgreSQL.
   - Crear Dockerfiles personalizados para Odoo y PostgreSQL.
   - Utilizar Docker Compose para definir la arquitectura de la aplicación multicontenedor.
   - Probar y verificar cada componente de la infraestructura Docker para garantizar su correcto funcionamiento.

   **Esquema Arquitectura**

   ![Azure -> Ubuntu -> Docker -> Odoo](img/azure-docker-odoo.png )

   **Etapa 2: Infraestructura:**

   Ansible es una herramienta de automatización open-source que permite la gestión y configuración de sistemas de forma eficiente y consistente. Utiliza un lenguaje descriptivo simple (YAML) para definir playbooks que describen los estados deseados de los sistemas, permitiendo la automatización de tareas complejas de forma sencilla.

   Azure Cloud Shell es un entorno de shell interactivo basado en la nube que proporciona una serie de herramientas preinstaladas, incluyendo Ansible. Con Ansible en Azure Cloud Shell, los usuarios pueden automatizar tareas relacionadas con la infraestructura en la nube de Azure de manera eficiente.

   Para utilizar Ansible en Azure Cloud Shell, es posible que sea necesario instalar módulos de Galaxy o collections adicionales dependiendo de los requisitos específicos del proyecto. Estos módulos pueden ser instalados fácilmente utilizando el comando `ansible-galaxy` seguido del nombre del módulo o la colección.

   Se ha elegido Ansible para este proyecto debido a su agilidad en la creación de los recursos necesarios en Azure. Ansible permite definir la infraestructura como código, lo que facilita la reproducibilidad y la automatización de tareas complejas de implementación.

   **Instrucciones para Usarlo en el Contexto de Este Proyecto:**
   1. Descargamos el repositorio en Azure Cloud Shell utilizando los comandos `git clone https://gitlab.com/MariPain/odoo-docker-azure-project.git`, luego `git pull` para descargar el lla ultima versión.
   2. Navegamos hasta el directorio `ansible/infra.playbook`.
   3. Ejecutar el playbook utilizando el comando `ansible-playbook infra.yml`. Este playbook se encargará de crear los recursos necesarios en Azure según lo definido en el archivo `infra.yml`.
   4. Una vez que el playbook haya completado la creación de recursos, conecte a la VM (denominada `odoo-service`) para continuar con la siguiente fase del despliegue de Odoo con contenedores Docker.

   El playbook se encarga de crear la infraestructura necesaria en Azure para desplegar la aplicación Odoo. Vamos a describir los recursos que se crean utilizando las variables definidas en el playbook:

   1. **Resource Group (Grupo de Recursos):**
      - Nombre: "OCC_ASD_Maria-Dolores"
      - Ubicación: "France Central"
      - Descripción: El grupo de recursos es un contenedor lógico que se utiliza para organizar y gestionar los recursos de Azure en un entorno. Agrupa los recursos que comparten el mismo ciclo de vida, permisos y políticas.

   2. **Virtual Network (Red Virtual):**
      - Nombre: "Vnet_LFT"
      - Rango de Direcciones: "10.0.16.0/24"
      - Descripción: Una red virtual en Azure permite aislar recursos en una red virtualizada. Esta red virtual proporciona conectividad entre los recursos desplegados en Azure.

   3. **Subnets (Subredes):**
      - Subred Frontend:
      - Nombre: "Subnet_Front"
      - Rango de Direcciones: "10.0.16.0/28"
      - Descripción: La subred divide una red virtual en segmentos más pequeños para gestionar el tráfico de red. La subred Frontend se puede utilizar para recursos públicos, mientras que puede haber otra subred destinada a los recursos privados. 

      En este contexto solo usaremos una subred en Azure ya que volveremos a segmentar subredes dentro de la Arquitectura de Docker.

   4. **Network Security Group (Grupo de Seguridad de Red):**
      - Nombre: "NSG_LFT"
      - Descripción: El grupo de seguridad de red actúa como un firewall virtual para controlar el tráfico de red hacia los recursos en Azure. Aquí se definen reglas de acceso para permitir o denegar el tráfico entrante y saliente.

   5. **Public IP Address (Dirección IP Pública):**
      - Nombre: "odoo-service-public-ip"
      - Método de Asignación: Estático
      - Descripción: La dirección IP pública se asigna a la interfaz de red de la VM para permitir la conectividad desde Internet.

   6. **Network Interface (Interfaz de Red):**
      - Nombre: "odoo-service-nic"
      - Descripción: La interfaz de red conecta la VM a la red virtual y proporciona conectividad de red para la VM.

   7. **Virtual Machine (Máquina Virtual):**
      - Nombre: "odoo-service"
      - Tamaño de la VM: "Standard_B2s"
      - Imagen: "Ubuntu Server 20.04 LTS"
      - Tipo de Disco Administrado: "Standard_LRS"
      - Descripción: La máquina virtual es el entorno de ejecución para la aplicación Odoo. Se crea con una VM Ubuntu y se configura con la interfaz de red, la dirección IP pública y la clave SSH necesarias para la conexión remota.

   De esta forma se automatizará la creación de la infraestructura necesaria en Azure utilizando Ansible en Azure Cloud Shell, lo que permitirá una implementación rápida y consistente del entorno requerido para el despliegue de Odoo.

   **Etapa 3: Despliegue Automatizado de Aplicación con Docker:**

   Una vez que la máquina virtual está creada y configurada, procedemos con el despliegue automatizado de la aplicación utilizando Docker.

   Para ello, nos conectaremos a  la VM `odoo-service` desde Azure Cloud Shell usando el comando `ssh mariola@[ip_vm]`. Una vez hayamos accedido a la maquina debemnos volver a clonar el repositorio que contiene las instrucciones. Nuevamente `git clone https://gitlab.com/MariPain/odoo-docker-azure-project.git`, luego `git pull` para descargar el lla ultima versión.
   Navegamos hasta el directorio `docker/` y ahi encontramos varios ficheros. EL primero que nos interesa es el script conficuracion llamado `docker_cnf.sh` que nos facilitara la actualizacion de repositorios, descarga e intalaciones necesarioas de Docker.

   1. **Automatización de la Implementación de Docker en la VM Ubuntu:**

      Para agilizar la instalación y configuración de Docker, así como la creación y conexión de los contenedores de Odoo y PostgreSQL, utilizaremos un script de automatización llamado `docker_cnf.sh`. Este script realiza las siguientes acciones:

      - Actualiza e instala los paquetes necesarios para trabajar con Docker.
      - Agrega la clave GPG oficial de Docker y configura el repositorio estable de Docker.
      - Instala Docker Engine y Docker Compose.
      - Crea redes Docker personalizadas (`app-network` y `db-network`) con subnets específicas.
      - Ejecuta los contenedores de Odoo y PostgreSQL, conectándolos a las redes personalizadas creadas.
      - Verifica que los contenedores estén en ejecución y conectados correctamente a las redes.


   3. **Creación de Dockerfiles Personalizados (si es necesario):**

      - Para la personalizacion de contenedores hemos creado Dockerfiles personalizados con el fin de  construir las imágenes a partir de ellos. Sin embargo, esto quiere decir, si furan necearias configuraciones adicuonales en las imagenes. En este proyecto se han creado dos `dockerfile` para ello, y seran usados para el despliegue del servicio.
      

   4. **Definición de la Arquitectura Multicontenedor con Docker Compose:**

      - Utilizaremos Docker Compose para definir la arquitectura multicontenedor de la aplicación Odoo. Para ello creamo un archivo llamado `docker-compose.yml` en el directorio de trabajo y defina los servicios de Odoo y PostgreSQL, incluyendo la configuración de red, volúmenes y variables de entorno necesarias para su funcionamiento adecuado. 

      - El archivo docker-compose.yml define la arquitectura multicontenedor de la aplicación Odoo. 

      - Se definen los servicios de Odoo y PostgreSQL, especificando la imagen Docker a utilizar para cada uno.

      - Configuración de Red y Puertos: Se configuran las redes app-network y db-network, asignando direcciones IP específicas a cada servicio.
      
      - Se mapea el puerto 80 del host al puerto 8069 del contenedor de Odoo para acceder a la aplicación desde el navegador.

      - Especificación de la asignación de direcciones IP para los servicios en las redes `app-network` y `db-network`.
      - Mapeo del puerto 80 del host al puerto 8069 del contenedor de Odoo para acceder a la aplicación desde el navegador.


   5. **Prueba y Verificación de la Infraestructura Docker:**
      - Una vez definida la arquitectura multicontenedor con Docker Compose, ejecute el comando `docker-compose up -d` para iniciar los contenedores en segundo plano.
      - Verifique que los contenedores estén en funcionamiento ejecutando el comando `docker-compose ps`.
      - Acceda a la interfaz web de Odoo desde un navegador utilizando la dirección IP de la VM y el puerto especificado en el archivo `docker-compose.yml` (por ejemplo, `http://<dirección_ip_vm>:80`). 
   
   **Consideraciones de seguridad para el proyecto:**

   1. **Grupo de Seguridad de Red (NSG):**
      - El Grupo de Seguridad de Red (NSG) en Azure es esencial para filtrar el tráfico hacia y desde los recursos en la nube, incluidas las máquinas virtuales y los contenedores Docker. Al configurar reglas en el NSG, se restringe el tráfico a lo necesario, como el acceso SSH desde direcciones IP específicas y el tráfico de la aplicación (por ejemplo, el puerto 8069 para Odoo). Esto ayuda a minimizar el riesgo de exposición a amenazas externas.

   2. **VNET y Subredes:**
      - Utilizar una Virtual Network (VNET) en Azure permite la creación de subredes para segmentar los recursos según su función y nivel de confianza. Las subredes ayudan a organizar los recursos y aplicar políticas de seguridad de forma granular. Además, las reglas del NSG se aplican a nivel de subred, lo que garantiza una mayor seguridad y control sobre el tráfico de red.

      - Las subredes Docker, como `app-network` y `db-network` en nuestro caso, actúan de manera similar a las subredes en Azure VNET. Permiten segmentar los servicios y contenedores en grupos lógicos y aplicar políticas de seguridad específicas a cada uno. Al crear estas subnets, estamos estableciendo un perímetro de seguridad adicional para nuestros servicios Docker, limitando la exposición a posibles amenazas externas.

   3. **Contenedores Docker:**
      - Es fundamental ejecutar los contenedores Docker con los privilegios mínimos necesarios para su correcto funcionamiento. Esto implica restringir los privilegios del usuario dentro del contenedor y evitar ejecutar contenedores con privilegios de root siempre que sea posible. Además, se deben seguir las mejores prácticas de seguridad al construir imágenes de Docker, como reducir al mínimo el número de capas, escanear en busca de vulnerabilidades conocidas y mantenerlas actualizadas regularmente.

   4. **Interfaz de Red (NIC):**
      - La Interfaz de Red (NIC) asociada a la máquina virtual o al contenedor Docker juega un papel crucial en la conectividad de red y la seguridad. Es importante configurar correctamente la NIC para utilizar el NSG adecuado y restringir el tráfico según las reglas definidas en el NSG. Esto garantiza que solo se permita el tráfico necesario y se limite la exposición a posibles ataques.

   5. **Redirección de Puertos:**
      - Cuando se necesite exponer servicios en contenedores Docker a través de puertos específicos, es fundamental configurar la redirección de puertos de manera segura. Esto implica limitar la exposición de puertos a lo estrictamente necesario y utilizar técnicas como la traducción de puertos dinámicos para evitar ataques de escaneo de puertos y reducir la superficie de ataque.

   6. **Conexión SSH:**
      - Para proteger la conexión SSH a la máquina virtual o al contenedor Docker, es recomendable utilizar claves SSH en lugar de contraseñas. Se deben generar y utilizar claves SSH fuertes, y protegerlas adecuadamente para prevenir el acceso no autorizado. Además, se recomienda restringir el acceso SSH solo a direcciones IP específicas y limitar el número de intentos de inicio de sesión para mitigar posibles ataques de fuerza bruta.
   **Securización adicional con Bastión (no implementada):**

   Si se quisieramos agregar una capa adicional de seguridad, se puede implementar un bastión en Azure. Un bastión es un servidor intermedio que actúa como punto de entrada seguro para acceder a máquinas virtuales y contenedores Docker en una red privada virtual (VNET) sin exponerlas directamente a Internet.

   Utilizar un bastión ayuda a reducir la superficie de ataque al proporcionar un único punto de entrada controlado y monitorizado para las conexiones SSH.

   Pero...

   **¿Qué es azure bastión?**
   Un bastión, también conocido como host de salto o jump host, es un servidor configurado para proteger y controlar el acceso a otros servidores en una red privada. Actúa como un punto de entrada único y seguro para administradores u operadores de sistemas que necesitan acceder a máquinas en la red interna desde una ubicación externa.

   **Funcionamiento bastión:**
   1. **Conexión inicial:** En lugar de asignar una dirección IP pública directamente a la VM (en este caso, `odoo-service`), utilizaremos el bastión como punto de entrada para acceder a la red privada donde reside la VM.
      
   2. **Conexión SSH a través de bastión:** Cuando un administrador desea acceder a la VM `odoo-service`, primero se conecta al bastión utilizando SSH desde una ubicación externa. El bastión autentica al administrador y verifica sus credenciales.

   3. **Redirección de puertos:** Una vez autenticado en el bastión, el administrador puede usar SSH para conectarse a la VM `odoo-service`, pero esta conexión se realiza a través del bastión. El bastión redirige el tráfico SSH entrante al puerto correspondiente de la VM `odoo-service`.

   4. **Seguridad adicional:** El bastión actúa como un punto de control de acceso centralizado, lo que permite implementar políticas de seguridad adicionales, como la autenticación de dos factores, registro de auditoría y filtrado de IP.

   **Beneficios del uso de bastión:**
   - **Mayor seguridad:** Al eliminar la exposición directa de la VM `odoo-service` a Internet, reducimos el riesgo de ataques maliciosos.
   - **Control de acceso:** El bastión proporciona un punto centralizado para administrar y auditar el acceso a los servidores internos.
   - **Simplificación de la seguridad:** Al concentrar las medidas de seguridad en un único punto de entrada, simplificamos la gestión y el mantenimiento de la seguridad de la infraestructura.

   **Implementación de bastión:**
   Para implementar un bastión en nuestra arquitectura, necesitaremos configurar un servidor adicional como bastión y ajustar las reglas de firewall y grupos de seguridad para permitir el tráfico SSH solo desde el bastión hacia la VM `odoo-service`. Además, configuraremos el bastión para redirigir el tráfico SSH entrante al puerto correspondiente de la VM `odoo-service`.

</details>

