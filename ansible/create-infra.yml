- name: Create Azure infrastructure (VNet, NSG, NIC, Public IP, and VM with Managed Disk)
  hosts: localhost
  connection: local
  gather_facts: no
  vars:
    location: "France Central"
    resource_group_name: "OCC_ASD_Maria-Dolores"
    vnet_name: "Vnet_LFT"
    subnet_front: "Subnet_Front"
    vnet_address_prefix: "10.0.16.0/24"
    subnet_front_prefix: "10.0.16.0/28"
    subnet_data_prefix: "10.0.16.16/28"
    nsg_name: "NSG_LFT"
    vm_name: "odoo-service"
    vm_publisher: "canonical"
    vm_offer: "0001-com-ubuntu-server-focal"
    vm_sku: "20_04-lts-gen2"
    vm_version: "latest"
    vm_size: "Standard_B2s"
    admin_username: "mariola"
    storage_account_name: "mpodoostorage"
    disk_name: "odoo-disk"
    disk_size_gb: 30
    public_ip_address: "{{ vm_name }}-public-ip"

  tasks:
    - name: Generate SSH key pair for connection
      ansible.builtin.shell:
        cmd: "ssh-keygen -t rsa -b 2048 -N '' -f ~/.ssh/id_rsa -C ''"
        creates: "~/.ssh/id_rsa"
      register: connection_key_result
      changed_when: false

    - name: Set the SSH public key variable
      set_fact:
        ssh_public_key: "{{ lookup('file', '~/.ssh/id_rsa.pub') }}"

    - name: Create SSH public key on Azure
      azure.azcollection.azure_rm_sshpublickey:
        resource_group: "{{ resource_group_name }}"
        public_key: "{{ ssh_public_key }}"
        name: "ssh-public-key"
        state: present
      register: ssh_publickey_result

    - name: Create NSG (Network Security Group)
      azure.azcollection.azure_rm_securitygroup:
        resource_group: "{{ resource_group_name }}"
        name: "{{ nsg_name }}"
        rules:
          - name: "Allow-SSH"
            protocol: Tcp
            destination_port_range: 22
            access: Allow
            priority: 1001
            direction: Inbound
          - name: "Allow-HTTP"
            protocol: Tcp
            destination_port_range: 80
            access: Allow
            priority: 1002
            direction: Inbound

    - name: Create VNet
      azure.azcollection.azure_rm_virtualnetwork:
        resource_group: "{{ resource_group_name }}"
        name: "{{ vnet_name }}"
        address_prefixes: "{{ vnet_address_prefix }}"

    - name: Create Subnets
      azure.azcollection.azure_rm_subnet:
        resource_group: "{{ resource_group_name }}"
        virtual_network_name: "{{ vnet_name }}"
        name:  "{{ subnet_front }}"
        address_prefix: "{{ item.prefix }}"
      
    - name: Create Public IP
      azure.azcollection.azure_rm_publicipaddress:
        resource_group: "{{ resource_group_name }}"
        allocation_method: Static
        name: "{{ public_ip_address }}"
      register: public_ip_result

    - name: Create Network Interface with Public IP
      azure.azcollection.azure_rm_networkinterface:
        resource_group: "{{ resource_group_name }}"
        name: "{{ vm_name }}-nic"
        virtual_network_name: "{{ vnet_name }}"
        subnet: "{{ subnet_front }}"
        security_group: "{{ nsg_name }}"
        public_ip_name: "{{ public_ip_address }}"

    - name: Set public_ip fact
      ansible.builtin.set_fact:
        public_ip: "{{ public_ip_result.state.ip_address }}"

    - name: Create Virtual Machine in Azure with NIC and Managed Disk
      azure.azcollection.azure_rm_virtualmachine:
        resource_group: "{{ resource_group_name }}"
        name: "{{ vm_name }}"
        vm_size: "{{ vm_size }}"
        location: "{{ location }}"
        admin_username: "{{ admin_username }}"
        ssh_password_enabled: false
        ssh_public_keys:
          - path: "/home/{{ admin_username }}/.ssh/authorized_keys"
            key_data: "{{ ssh_public_key }}"
        network_interface_names: "{{ vm_name }}-nic"
        image:
          publisher: "{{ vm_publisher }}"
          offer: "{{ vm_offer }}"
          sku: "{{ vm_sku }}"
          version: "{{ vm_version }}"
        managed_disk_type: "Standard_LRS"
        state: present
